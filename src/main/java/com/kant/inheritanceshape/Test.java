/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kant.inheritanceshape;

/**
 *
 * @author MSI
 */
public class Test {
    public static void main(String[] args) {
        Circle circle1 = new Circle(3);
        Circle circle2 = new Circle(4);
        Rectangle rectangle = new Rectangle(3,4);
        Square square = new Square (2);
        Triangle triangle = new Triangle(4,3);
        
        Shape[] shape ={circle1,circle2,rectangle,square,triangle};
        for(int i=0; i<shape.length; i++){
            shape[i].Show();
            
        }
    }
    
}
